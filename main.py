import pymongo
import os
import io
import dns
import requests
import time
from bs4 import BeautifulSoup
from tkinter import *
from selenium import webdriver  
from selenium.common.exceptions import NoSuchElementException  
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options

root = Tk()
topFrame = Frame(root)
bottomFrame = Frame(root)
label1 = Label(topFrame, text="Witaj")
label2 = Label(topFrame, text="podaj Id produktu")
label3 = Label(topFrame, text="Num")
label4 = Label(topFrame, text="Title")
label5 = Label(topFrame, text="Rate")
text = Text(bottomFrame)
txt = Entry(topFrame, width=10, text="8526729")

headers = {"User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0'}
client = pymongo.MongoClient("mongodb+srv://admin:admin1@projekt-yuceo.mongodb.net/test?retryWrites=true&w=majority")
db = client.projekt
count = 1
id = "8526729"
check_E = False
check_T = False
col = db[id]

def getIds():
  """A method to get new Product ID."""
  global id, col, count
  id = txt.get()
  if id == "":
    label1.config(text="Podaj ID produktu")
  else:
    id = str(id)
    col = db[id]
    count = 1
    label1.config(text="Id zostało zmienieone")



def getHTML():
  """A method for exporting content of the pages."""
  global URL, page, soup, check_E, driver, html_source, soup_opp, collist
  URL = "https://www.decathlon.pl/klapki-slap-100-basic-id_" +id+".html"
  page = requests.get(URL, headers=headers)
  soup = BeautifulSoup(page.content, 'html.parser') 
  options = Options()
  options.add_argument('--headless')
  driver = webdriver.Firefox(options=options)
  url2 = "https://www.decathlon.pl/pl/pageReviews?productId=" +id+"&collaborator=0"
  driver.get(url2)
  scroll_down()
  html_source = driver.page_source
  driver.quit()
  label1.config(text="Dane pobrane")
  check_E = True
  
def scroll_down():
  """A method for scrolling the page."""
  # Get scroll height
  last_height = driver.execute_script("return document.body.scrollHeight")

  while True:
    # Scroll down to bottom
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Wait to load page
    time.sleep(2)

    # Calculate new scroll height and compare with last scroll height
    new_height = driver.execute_script("return document.body.scrollHeight")
    if new_height == last_height:
        break
    last_height = new_height


def change():
  """A method to convert row data."""
  global soup_opp, count, name, price, opinion_count, rating, string, collist, check_T
  if check_E == True:
    collist = db.list_collection_names()
    if id in collist:
      label1.config(text="dany produkt istnieje już w bazie danych")
    else:
      name = soup.find(attrs="title-product").get_text()
      price = soup.find(attrs="price").get_text()
      opinion_count = soup.find(attrs="review-count")
      if opinion_count == None:
        opinion_count = "(0)"
        string = {"num": "0", "Product main info": "Basic data of the product", "product_id": id, "name": name, "Price": price.strip(), "Rate": 0, "Opinions": opinion_count}
        check_T
        label1.config(text="Brak opini")
        check_T = True
      else:
        rating = soup.find(attrs="rating-note-container").get_text() 
        string = {"num": "0", "Product main info": "Basic data of the product", "product_id": id, "name": name, "Price": price.strip(), "Rate": rating, "Opinions": opinion_count.get_text().strip()}
        transferop()
      check_T - True
      
  else:
    label1.config(text="Wykonaj krok Export")
    check_T = False
  
def transferop():
  """A method to convert row data in opinions section."""
  global opinions, article, title, opis, rate, info, date, stringopp, check_T, col, count, soup_opp
  soup_opp = BeautifulSoup(html_source, 'html.parser')
  opinions = soup_opp.find(attrs="reviews-list-container")
  article = opinions.find_all("article",  "review-container")
  for z in article:
    title  = z.find(attrs="r-title-resume").get_text()
    opis = z.find(attrs="r-desc").get_text()
    rate = z.find(attrs="r-note-resume").get_text()
    info = z.find(attrs="r-user-firstname").get_text().strip()
    date = z.find(attrs="r-date").get_text()
    stringopp = {"num": count, "product_id": id, "Title": title, "Description": opis, "Rate": rate, "Added by": info, "When added": date}
    insert = col.insert_one(stringopp)
    count += 1
  check_T = True
  label1.config(text="Dane przekonwertowane")
   
def insert():
  """A method to transfer data into DB"""
  global insert, check_T, check_E
  if check_T == True:
    col = db[id]
    insert = col.insert_one(string)
    label1.config(text="Dodano " + str(count) + " rekordów")
    check_T = False
    check_E = False
  else:
    label1.config(text="Wykonaj krok Transform")
  
def main():
  collist = db.list_collection_names()
  if id in collist:
    label1.config(text="dany produkt istnieje już w bazie danych")
  else:
    getHTML()
    change()
    insert()
    label1.config(text="Dodano " + str(count) + " rekordów")
  
def drop_collections():
  """A method to delete all collections from DB"""
  collist = db.list_collection_names()
  for x in collist:
    mycol = db[x]
    mycol.drop()
  label1.config(text="Udało się! Brak kolekcji")
  if collist == []:
    label1.config(text="Brak kolekcji w bazie, dodaj nowe produkty")

def printing():
  """A method  all collections and it content from DB"""
  text.delete(1.0, END)
  collist = db.list_collection_names()
  if collist == []:
    label1.config(text="Brak kolekcji, proszę zaktualizuj bazę")
  else:
    for x in collist:
      csv = db[x]
      for doc in csv.find():
        doc = str(doc)
        doc = doc.replace(",","\n")
        doc = doc.replace("'","")
        text.insert(INSERT, doc + "\n\n")

def printing_1():
  """A method  all collections and it content from DB"""
  text.delete(1.0, END)
  collist = db.list_collection_names()
  if collist == []:
    label1.config(text="Brak kolekcji, proszę zaktualizuj bazę")
  else:
    csv = db[id]
    for doc in csv.find():
        doc = str(doc)
        doc = doc.replace(",","\n")
        doc = doc.replace("'","")
        text.insert(INSERT, doc + "\n\n")

def printing_col():
  """A method  all collections from DB"""
  text.delete(1.0, END)
  csv = db[id]
  collist = db.list_collection_names()
  text.insert(INSERT, "Lista produktów w bazie danych\n")
  if collist == []:
    label1.config(text="Brak kolekcji, proszę zaktualizuj bazę")
  for doc in collist:
    doc = str(doc)
    doc = doc.replace(",","\n")
    text.insert(INSERT, doc + "\n")
    
def sort_num_asc():
  collist = db.list_collection_names()
  sort = db[id].find().sort("num", 1)
  text.delete(1.0, END)
  if collist == []:
    label1.config(text="Brak kolekcji, proszę zaktualizuj bazę")
  else:
    for x in sort:
      x = str(x)
      x = x.replace(",","\n")
      text.insert(INSERT, x + "\n\n")

def sort_num_desc():
  collist = db.list_collection_names()
  sort = db[id].find().sort("num", -1)
  text.delete(1.0, END)
  if collist == []:
    label1.config(text="Brak kolekcji, proszę zaktualizuj bazę")
  else:
    for x in sort:
      x = str(x)
      x = x.replace(",","\n")
      text.insert(INSERT, x + "\n\n")

def sort_rate_asc():
  collist = db.list_collection_names()
  sort = db[id].find().sort("Rate", 1)
  text.delete(1.0, END)
  if collist == []:
    label1.config(text="Brak kolekcji, proszę zaktualizuj bazę")
  else:
    for x in sort:
      x = str(x)
      x = x.replace(",","\n")
      text.insert(INSERT, x + "\n\n")

def sort_rate_desc():
  collist = db.list_collection_names()
  sort = db[id].find().sort("Rate", -1)
  text.delete(1.0, END)
  if collist == []:
    label1.config(text="Brak kolekcji, proszę zaktualizuj bazę")
  else:
    for x in sort:
      x = str(x)
      x = x.replace(",","\n")
      text.insert(INSERT, x + "\n\n")
def sort_title_asc():
  collist = db.list_collection_names()
  sort = db[id].find().sort("Title", 1)
  text.delete(1.0, END)
  if collist == []:
    label1.config(text="Brak kolekcji, proszę zaktualizuj bazę")
  else:
    for x in sort:
      x = str(x)
      x = x.replace(",","\n")
      text.insert(INSERT, x + "\n\n")

def sort_title_desc():
  collist = db.list_collection_names()
  sort = db[id].find().sort("Title", -1)
  text.delete(1.0, END)
  if collist == []:
    label1.config(text="Brak kolekcji, proszę zaktualizuj bazę")
  else:
    for x in sort:
      x = str(x)
      x = x.replace(",","\n")
      text.insert(INSERT, x + "\n\n")
def csv():
  f = open('C:\\Users\\socze\\OneDrive\\Desktop\\mongodb.csv', "w")
  collist = db.list_collection_names()
  for x in collist:
    csv = db[x]
    for doc in csv.find():
      f.write(str(doc)+ "\n")
  label1.config(text="plik został zapisany")
  f.close()

#-----------------GUI-------------------------------------------------
button1 = Button(topFrame, text="ETL", fg="red", command=main, width=1)
button2 = Button(topFrame, text="E", command=getHTML)
button3 = Button(topFrame, text="T", command=change)
button4 = Button(topFrame, text="L", command=insert)
button5 = Button(topFrame, text="Wyświetl", command=printing)
button6 = Button(topFrame, text="Usun wszystkie kolekcje", command=drop_collections)
button7 = Button(topFrame, text="Zapisz do csv", command=csv)
button8 = Button(topFrame, text="wyświetl kolekcje", command=printing_col)
button9 = Button(topFrame, text="Sortowanie\nrosnące", command=sort_num_asc)
button10 = Button(topFrame, text="Zmiana ID", command=getIds)
button11 = Button(topFrame, text="Sortowanie\nmalejące", command=sort_num_desc)
button12 = Button(topFrame, text="Sortowanie\nrosnące", command=sort_rate_asc)
button13 = Button(topFrame, text="Sortowanie\nmalejące", command=sort_rate_desc)
button14 = Button(topFrame, text="Sortowanie\nrosnące", command=sort_title_asc)
button15 = Button(topFrame, text="Sortowanie\nmalejące", command=sort_title_desc)
button16 = Button(topFrame, text="Wyświetl 1\n kolekcje", command=printing_1)



topFrame.pack(side=LEFT)
bottomFrame.pack(side=RIGHT)
button1.grid(row = 1, column = 0, columnspan = 3, sticky='nesw')
button2.grid(row = 2, column = 0, pady = 2, sticky='nesw')
button3.grid(row = 2, column = 1, pady = 2, sticky='nesw')
button4.grid(row = 2, column = 2, pady = 2, sticky='nesw')
button5.grid(row = 3, column = 1, pady = 2, sticky='nesw')
button6.grid(row = 3, column = 2, pady = 2, sticky='nesw')
button7.grid(row = 3, column = 0, pady = 2, sticky='nesw')
button8.grid(row = 4, column = 0, pady = 2, sticky='nesw')
button9.grid(row = 9, column = 0, pady = 2, sticky='nesw')
button10.grid(row = 7, column = 0, pady = 2, sticky='nesw', columnspan = 3)
button11.grid(row = 10, column = 0, pady = 2, sticky='nesw')
button12.grid(row = 9, column = 2, pady = 2, sticky='nesw')
button13.grid(row = 10, column = 2, pady = 2, sticky='nesw')
button14.grid(row = 9, column = 1, pady = 2, sticky='nesw')
button15.grid(row = 10, column = 1, pady = 2, sticky='nesw')
button16.grid(row = 4, column = 1, pady = 2, sticky='nesw')
label1.grid(row = 0, column = 0, columnspan = 3, sticky='nesw')
label2.grid(row = 6, column = 0, columnspan = 2, sticky='nesw')
label3.grid(row = 8, column = 0, sticky='nesw')
label4.grid(row = 8, column = 1, sticky='nesw')
label5.grid(row = 8, column = 2, sticky='nesw')
txt.grid(column=2, row=6, columnspan = 2, sticky='nesw')
text.pack()


root.mainloop()

